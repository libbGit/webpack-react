import create from 'zustand';
import { persist } from 'zustand/middleware';
/**
 * 除了使用set函数，获取到外部的state之外，还可以使用get获取到外部的state
 */
const useUserStore = create(
  persist(
    (set, get) => ({
      loginTimes: 10, //登录次数

      //通过 set函数修改 state
      increaseTimes: () => set((state) => ({ loginTimes: state.loginTimes + 1 })),
      removeAllTimes: () => set({ loginTimes: 0 }),

      fetchTimes: async (pond) => {
        const loginTimes = get().loginTimes; //不用 set函数，而是通过get函数获取外部的 state
        const response = await fetch(pond);
        set({ loginTimes: await response.json() });
      },
    }),
    {
      name: 'user-storage', // unique name
      getStorage: () => window.sessionStorage, // (optional) by default, 'localStorage' is used
    }
  )
);

export default useUserStore;
