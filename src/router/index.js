import { createBrowserRouter, Route } from 'react-router-dom';
import App from '@/App';
// import Hello from '@/components/Hello';
// import World from '@/components/World';

const router = createBrowserRouter([
  // {
  //   path: '/',
  //   element: <App />,
  //   children: [
  //     {
  //       path: 'hello',
  //       element: <Hello />,
  //     },
  //     {
  //       path: 'world',
  //       element: <World />,
  //     },
  //   ],
  // },
]);

export default router;
