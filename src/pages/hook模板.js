import React, { useState, useEffect, useMemo, useRef, useContext } from 'react';
import PropTypes from 'prop-types';
import useUserStore from '@/store/user';
import { ThemeContext } from '../middleware/Content';
import { Button } from 'antd';

/**
只在 React 函数式组件中调用 Hook，不要在普通的 JavaScript 函数中调用 Hook
只能在函数最外层调用 Hook。不要在循环、条件判断或者子函数中调用。

useEffect 会在每次渲染后都执行，默认情况下，它在第一次渲染之后和每次更新之后都会执行
 */
function Hello(props) {
  //使用 useState钩子定义state，用其他方式定义的不是state。state或者props改变后，会重新执行return的渲染
  let [state, setState] = useState({
    name: 'hello',
    isShow: false,
  });

  //ref
  const myRef = useRef(null);

  //store
  const { loginTimes, increaseTimes, removeAllTimes } = useUserStore();

  //context
  let context = useContext(ThemeContext);

  //类似于computed
  let introduceStr = useMemo(() => {
    return `my name is ${state.name}, my age is ${state.isShow}`;
  }, [state]);

  //methods
  let handleAdd = () => {
    increaseTimes(loginTimes + 1);
  };
  let handleRemove = () => {
    removeAllTimes();
  };

  // 副作用hook（数据获取、订阅或者手动修改过 DOM），告诉 React 在完成组件渲染后运行你的“副作用”函数，
  useEffect(() => {
    //todo
    return function cleanup() {
      //todo
    };
  }, [state]); //仅在 state 更改时更新

  //每次渲染后调用副作用函数
  useEffect(() => {
    document.title = state.name + 1;
    return function cleanup() {
      //todo
    };
  });

  let callback = (e) => {
    console.log('aaaa', e);
  };

  let handleClick = (e) => {
    console.log('context', context.theme);
    //hook中的setState没有第二个回调参数，需要在副作用hook中处理.而且setState会全量替换，所以要携带以前的
    setState({
      ...state,
      isShow: !state.isShow, //只会更新当前变量，其他变量不会被覆盖
    });
  };

  const numbers = [1, 2, 3, 4, 5];
  return (
    <React.Profiler id="Navigation" onRender={(...e) => callback(e)}>
      <div>
        <button onClick={(e) => handleClick(e)}>click me</button>
        <h1>state name, {state.name}</h1>
        <h2>props name {props.name}.</h2>
        <div>
          条件渲染
          {state.isShow ? <div>这是显示得</div> : <div>这是不显示得</div>}
        </div>

        <div>
          列表渲染
          {numbers.map((number) => (
            // 这里返回的东西要是个jsx才行
            <li key={number.toString()}>{number}</li>
          ))}
        </div>
        <div>
          表单尽量用受控组件，不要用非受控
          {/* <input value={this.state.value} onChange={this.handleChange} / */}
        </div>

        <div>
          绑定ref, myRef.current是个dom实例
          <div ref={myRef} onClick={(e) => console.log(myRef.current)}>
            点击
          </div>
        </div>

        {/* hook中无 render prop，可以使用另一个hook解决 */}
        {/* {props.render(state)} */}

        {/* 外部 children prop */}
        <div>{props.children({ x: 1 })}</div>

        <div>introduce =={introduceStr}</div>

        <input ref={myRef} type="text"></input>

        <div>
          <div>登录次数={loginTimes}</div>

          <Button type="primary" onClick={handleAdd}>
            add times
          </Button>
          <Button type="danger" onClick={handleRemove}>
            remove times
          </Button>
        </div>
      </div>
    </React.Profiler>
  );
}

//指定类型
Hello.propTypes = {
  name: PropTypes.string, // array, bool, func, number, object, symbol, element
};
// 指定 props 的默认值：
Hello.defaultProps = {
  name: 'Stranger',
};

export default Hello;
