import React from 'react'; //react作用域必须存在于组件中，即使不用，是核心
import ReactDOM from 'react-dom'; //操作dom部分
import PropTypes from 'prop-types';

import { ThemeContext } from '../middleware/Content';

class World extends React.Component {
  //state固定实例属性，可通过setState修改
  state = {
    //定义state
    name: 'hello',
    isShow: false,
  };

  myRef = React.createRef(); //创建一个ref,不能在函数组件上使用 ref 属性，因为他们没有实例。

  handleClick(e) {
    console.log('context', this.context.theme);
    this.setState(
      {
        isShow: !this.state.isShow, //只会更新当前变量，其他变量不会被覆盖
      },
      () => {
        //setstate执行后，dom渲染完成执行此处，类似nexttick
        console.log(11);
      }
    );
  }

  callback(e) {
    console.log('aaaa', e);
  }

  //如果 return null,则组件被隐藏，但是当前组件还是会进入，只是不显示。如果不想让当前组件彻底不渲染，要在父组件进行条件判断
  render() {
    const numbers = [1, 2, 3, 4, 5];
    return (
      <React.Profiler id="Navigation" onRender={(...e) => this.callback(e)}>
        <div>
          <button onClick={(e) => this.handleClick(e)}>click me</button>
          <h1>state name, {this.state.name}</h1>
          <h2>props name {this.props.name}.</h2>
          <div>
            条件渲染
            {this.state.isShow ? <div>这是显示得</div> : <div>这是不显示得</div>}
          </div>
          <div>
            列表渲染
            {numbers.map((number) => (
              // 这里返回的东西要是个jsx才行
              <li key={number.toString()}>{number}</li>
            ))}
          </div>
          <div>
            表单尽量用受控组件，不要用非受控
            {/* <input value={this.state.value} onChange={this.handleChange} / */}
          </div>
          <div>
            绑定ref, myRef.current是个dom实例
            <div ref={this.myRef} onClick={(e) => console.log(this.myRef.current)}>
              点击
            </div>
          </div>

          {/* 外部 render prop */}
          {this.props.render(this.state)}
          {/* 外部 children prop */}

          <div>{this.props.children({ x: 1 })}</div>
        </div>
      </React.Profiler>
    );
  }

  //dom已渲染完
  componentDidMount() {
    console.log('this.myRef.current', this.myRef.current);
  }

  shouldComponentUpdate() {
    return true;
    //如果返回了false，React不会去执行render渲染
  }
  //组件即将被卸载
  componentWillUnmount() {
    //清楚定时器，http请求等行为
  }
}

World.contextType = ThemeContext; //contextType固定静态属性，接收创建的context，下文this.context才能拿到值
//指定类型
World.propTypes = {
  name: PropTypes.string, // array, bool, func, number, object, symbol, element
};
// 指定 props 的默认值：
World.defaultProps = {
  name: 'Stranger',
};

export default World;
